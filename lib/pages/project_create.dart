import 'package:flutter/material.dart';
import 'package:project_dev_manager/database_class/project_database.dart';
import 'package:project_dev_manager/models/project_model.dart';

class ProjectCreate extends StatelessWidget {
  //TODO create project
  const ProjectCreate({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Create project'),
      ),
      body: CreateProjectForm(),
    );
  }
}

class CreateProjectForm extends StatefulWidget {
  const CreateProjectForm({Key? key}) : super(key: key);

  @override
  _CreateProjectFormState createState() => _CreateProjectFormState();
}

class _CreateProjectFormState extends State<CreateProjectForm> {
  ProjectDatabase _projectDatabase = ProjectDatabase();

  TextEditingController nameProject = TextEditingController();
  GlobalKey<FormState> form = GlobalKey<FormState>();

  TextFormField inputCreate({TextEditingController? input, String? nameLabel}) {
    return TextFormField(
      controller: input,
      decoration:
          InputDecoration(border: OutlineInputBorder(), labelText: nameLabel),
      validator: (value) {
        if (value!.isEmpty) {
          return 'Campo vacio';
        }
        return null;
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Form(
        key: form,
        child: SingleChildScrollView(
          child: Column(
            children: [
              inputCreate(
                  input: nameProject, nameLabel: 'Enter name of project'),
              DatePickerDialog(
                  cancelText: 'Cancel',
                  confirmText: 'Accept',
                  initialDate: DateTime.now(),
                  firstDate: DateTime.now(),
                  lastDate: DateTime(2030)),
              ElevatedButton.icon(
                  onPressed: () {
                    if (form.currentState!.validate()) {
                      print(nameProject.text);
                      Map<String, dynamic> dataProject = ProjectsModel(
                              dateProject: DateTime.now(),
                              nameProject: nameProject.text)
                          .toMapProject();
                      _projectDatabase.insertData(
                          projectsModel:
                              ProjectsModel.fromJson(data: dataProject));
                      Navigator.of(context).pop();
                    }
                  },
                  icon: Icon(Icons.save),
                  label: Text('Save project'))
            ],
          ),
        ));
  }
}
