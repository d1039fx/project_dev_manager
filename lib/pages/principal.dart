import 'package:flutter/material.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:hive/hive.dart';
import 'package:project_dev_manager/database_class/project_database.dart';
import 'package:project_dev_manager/models/project_model.dart';
import 'package:project_dev_manager/pages/project_create.dart';

class Principal extends StatelessWidget {
  Principal({Key? key}) : super(key: key);

  void createProject(BuildContext context) {
    Navigator.of(context)
        .push(MaterialPageRoute(builder: (context) => ProjectCreate()));
  }

  static ProjectDatabase _projectDatabase = ProjectDatabase();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Manager Dev Projects'),
      ),
      //body: _showListProject(context),
      body: ValueListenableBuilder<Box>(
          valueListenable: _projectDatabase.dataProject(nameBox: 'projects').listenable(),
          builder: (context, box, widget) {
            return ListView.builder(itemCount: box.length,itemBuilder: (context, index){
              ProjectsModel dataProject = box.getAt(index);
              return ListTile(
                title: Text(dataProject.nameProject!),
                subtitle: Text(dataProject.dateProject!.toIso8601String()),
                onTap: (){
                  box.clear();
                },
              );
            });
          }),
      floatingActionButton: FloatingActionButton(
        onPressed: () => createProject(context),
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ),
    );
  }
}
