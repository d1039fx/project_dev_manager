import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:path_provider/path_provider.dart';
import 'package:project_dev_manager/database_class/project_database.dart';
import 'package:project_dev_manager/pages/principal.dart';

import 'models/project_model.dart';

void main() async {
  if(Platform.isLinux){
    Directory path = await getApplicationDocumentsDirectory();
    await Hive.initFlutter(path.path);
    Hive.registerAdapter(ProjectsModelAdapter());
  }else if(kIsWeb){
    Directory path = await getApplicationDocumentsDirectory();
    await Hive.initFlutter(path.path);
    Hive.registerAdapter(ProjectsModelAdapter());
  }
  else{
    await Hive.initFlutter();
    Hive.registerAdapter(ProjectsModelAdapter());
  }
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Manager Dev Projects',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: FutureBuilder<Box>(
          future: ProjectDatabase().initDB(nameDB: ''),
          builder: (context, snapshot) {
            if (snapshot.hasError) return Text(snapshot.error.toString());
            if (snapshot.connectionState == ConnectionState.done) {
              return Principal();
            } else {
              return Center(
                child: CircularProgressIndicator(),
              );
            }
          }),
    );
  }
}
