

import 'package:hive_flutter/hive_flutter.dart';

import 'package:project_dev_manager/models/project_model.dart';

class ProjectDatabase {
  Future<Box> initDB({required String nameDB}) async {
    switch (nameDB) {
      default:
        return await Hive.openBox('projects');
    }
  }

  Box dataProject({required String nameBox}) {
    return Hive.box(nameBox);
  }

  void insertData({ProjectsModel? projectsModel}) {
    dataProject(nameBox: 'projects').add(projectsModel);
  }
}
