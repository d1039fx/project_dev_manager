import 'package:hive_flutter/hive_flutter.dart';

part 'project_model.g.dart';

@HiveType(typeId: 0)
class ProjectsModel {
  @HiveField(0)
  String? nameProject;

  @HiveField(1)
  DateTime? dateProject;

  ProjectsModel({this.nameProject, this.dateProject});

// final String? nameProject;
  // final DateTime? dateProject;

  // ProjectsModel({this.nameProject, this.dateProject});
  //
  Map<String, dynamic> toMapProject() =>
      {'name_project': nameProject, 'date_project': dateProject};

  factory ProjectsModel.fromJson({required Map<String, dynamic> data}) =>
      ProjectsModel(
          nameProject: data['name_project'], dateProject: data['date_project']);
}
