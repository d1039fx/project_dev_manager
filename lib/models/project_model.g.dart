// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'project_model.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class ProjectsModelAdapter extends TypeAdapter<ProjectsModel> {
  @override
  final int typeId = 0;

  @override
  ProjectsModel read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return ProjectsModel()
      ..nameProject = fields[0] as String?
      ..dateProject = fields[1] as DateTime?;
  }

  @override
  void write(BinaryWriter writer, ProjectsModel obj) {
    writer
      ..writeByte(2)
      ..writeByte(0)
      ..write(obj.nameProject)
      ..writeByte(1)
      ..write(obj.dateProject);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is ProjectsModelAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
