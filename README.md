# Project Dev Manager

Este proyecto nace con la finalidad de gestionar proyectos personales.

## Estructura

<img src="https://gitlab.com/d1039fx/project_dev_manager/-/raw/master/images_readme/img_1.jpg"  width="640" height="480">

<img src="https://gitlab.com/d1039fx/project_dev_manager/-/raw/master/images_readme/img_2.jpg"  width="640" height="480">

<img src="https://gitlab.com/d1039fx/project_dev_manager/-/raw/master/images_readme/img_3.jpg"  width="640" height="480">

## Modelo de datos

<img src="https://gitlab.com/d1039fx/project_dev_manager/-/raw/master/images_readme/img_4.jpg"  width="640" height="480">

<img src="https://gitlab.com/d1039fx/project_dev_manager/-/raw/master/images_readme/img_5.jpg"  width="640" height="480">

## GUI

### Movil
<img src="https://gitlab.com/d1039fx/project_dev_manager/-/raw/master/images_readme/img_6.jpg"  width="640" height="480">

### Web/Desktop

<img src="https://gitlab.com/d1039fx/project_dev_manager/-/raw/master/images_readme/img_7.jpg"  width="640" height="480">

